# Übung 02

## Teilaufgabe 1

Berechne den Preis einer Warensendung.
Die Sendung bestehe aus n Stück eines Produkts mit dem Preis
p (in Euro), einzugeben sind n und p.
Dazu kommen Versandkosten:
* liegt der Warenwert unter 50 Euro, dann 7 Euro
* liegt der Warenwert unter 100 Euro (aber größer oder
gleich 50 Euro), dann 3,50 Euro
* ab 100 Euro versandkostenfrei

## Teilaufgabe 2
Berechne die Lösungen (Nullstellen, Wurzeln) einer
quadratischen Gleichung
**$ax^2+bx+c=0$**
in Abhängigkeit von a, b und c.
Hinweis: zur Berechnung der Quadratwurzel einer Zahl z kann
die Funktion sqrt(z) benutzt werden.
Beachte: Es gibt nicht immer zwei reelle Lösungen.
