// "Aufgabe2.cpp": Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"

#include <stdbool.h>
#include <stdio.h>
#include <conio.h>
#include <float.h>
#include <math.h>
#include <windows.h>

/*
HANDLE consoleOutputHandle(void)
{
  const DWORD nStdHandle = -11;
  return GetStdHandle(nStdHandle);
}

private static Win32Native.CONSOLE_SCREEN_BUFFER_INFO GetBufferInfo(bool throwOnNoConsole, out bool succeeded)
{
  succeeded = false;
  IntPtr consoleOutputHandle = Console.ConsoleOutputHandle;
  if (!(consoleOutputHandle == Win32Native.INVALID_HANDLE_VALUE))
  {
    Win32Native.CONSOLE_SCREEN_BUFFER_INFO cONSOLE_SCREEN_BUFFER_INFO;
    if (!Win32Native.GetConsoleScreenBufferInfo(consoleOutputHandle, out cONSOLE_SCREEN_BUFFER_INFO))
    {
      bool consoleScreenBufferInfo = Win32Native.GetConsoleScreenBufferInfo(Win32Native.GetStdHandle(-12), out cONSOLE_SCREEN_BUFFER_INFO);
      if (!consoleScreenBufferInfo)
      {
        consoleScreenBufferInfo = Win32Native.GetConsoleScreenBufferInfo(Win32Native.GetStdHandle(-10), out cONSOLE_SCREEN_BUFFER_INFO);
      }
      if (!consoleScreenBufferInfo)
      {
        int lastWin32Error = Marshal.GetLastWin32Error();
        if (lastWin32Error == 6 && !throwOnNoConsole)
        {
          return default(Win32Native.CONSOLE_SCREEN_BUFFER_INFO);
        }
        __Error.WinIOError(lastWin32Error, null);
      }
    }
    if (!Console._haveReadDefaultColors)
    {
      Console._defaultColors = (byte)(cONSOLE_SCREEN_BUFFER_INFO.wAttributes & 255);
      Console._haveReadDefaultColors = true;
    }
    succeeded = true;
    return cONSOLE_SCREEN_BUFFER_INFO;
  }
  if (!throwOnNoConsole)
  {
    return default(Win32Native.CONSOLE_SCREEN_BUFFER_INFO);
  }
  throw new IOException(Environment.GetResourceString("IO.IO_NoConsole"));
}

CONSOLE_SCREEN_BUFFER_INFO GetBufferInfo()
{
  bool flag;
  return Console.GetBufferInfo(true, out flag);
}

void clrscr()
{
  Win32Native.COORD cOORD = default(Win32Native.COORD);
  IntPtr consoleOutputHandle = Console.ConsoleOutputHandle;
  if (consoleOutputHandle == Win32Native.INVALID_HANDLE_VALUE)
  {
    throw new IOException(Environment.GetResourceString("IO.IO_NoConsole"));
  }
  Win32Native.CONSOLE_SCREEN_BUFFER_INFO bufferInfo = Console.GetBufferInfo();
  int num = (int)(bufferInfo.dwSize.X * bufferInfo.dwSize.Y);
  int num2 = 0;
  if (!Win32Native.FillConsoleOutputCharacter(consoleOutputHandle, ' ', num, cOORD, out num2))
  {
    __Error.WinIOError();
  }
  num2 = 0;
  if (!Win32Native.FillConsoleOutputAttribute(consoleOutputHandle, bufferInfo.wAttributes, num, cOORD, out num2))
  {
    __Error.WinIOError();
  }
  if (!Win32Native.SetConsoleCursorPosition(consoleOutputHandle, cOORD))
  {
    __Error.WinIOError();
  }
}
//*/

// Gets a double value from console.
double consoleGetDouble(char *title)
{
  double d;
  printf(title);
  scanf_s("%lf", &d);
  return d;
}

void exerciseA(void)
{
  printf("Bitte die im Folgenden erfragten Daten eingeben...\n");
  double quantity = consoleGetDouble("Menge des Artikels:                 ");
  double price    = consoleGetDouble("Preis je Artikeleinheit (in Euro):  ");
  double valueOfGoods = quantity * price;
  double shippingCosts = (valueOfGoods < 50 ? 7 : (valueOfGoods < 100 ? 3.5 : 0));
  double totalCosts = valueOfGoods + shippingCosts;

  printf("----------------------------------------\n");
  printf("Artikelmenge:   %10.2lf Euro\n", quantity);
  printf("Artikelpreis:   %10.2lf Euro\n", price);
  printf("Warenwert:      %10.2lf Euro\n", valueOfGoods);
  printf("Versandkosten:  %10.2lf Euro\n", shippingCosts);
  printf("Gesamtkosten:   %10.2lf Euro\n", totalCosts);

  _getch();
  //clrscr();
  system("cls");
}

bool quadraticEquation(double a, double b, double c, double *x1, double *x2)
{
  double discriminant = sqrt(pow(b, 2) - (4 * a * c));
  if (isnan(discriminant))
  {
    return false;
  }
  *x1 = (-b + discriminant) / (2 * a);
  *x2 = (-b - discriminant) / (2 * a);
  return true;
}

void exerciseB(void)
{
  printf("Zur Loesung der Formel \"ax^2 + bx + c = 0\" bitte die Koeffizienten eingeben...\n");
  double a = consoleGetDouble("a: ");
  double b = consoleGetDouble("b: ");
  double c = consoleGetDouble("c: ");
  double x1;
  double x2;
  if (quadraticEquation(a, b, c, &x1, &x2))
  {
    printf("Koeffizient a = %10.2lf\n", a);
    printf("Koeffizient b = %10.2lf\n", b);
    printf("Koeffizient c = %10.2lf\n", c);
    printf("Loesung x1 = %10.2lf\n", x1);
    printf("Loesung x2 = %10.2lf\n", x2);
  }
  else
  {
    printf("Keine Loesung (ohne Komplexe Zahlen) moeglich!\n");
  }

  _getch();
  //clrscr();
  system("cls");
}

int main(void)
{
  exerciseA();
  exerciseB();

  return 0;
}
